﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business_Layer
{
    public static class GestionnaireSexe
    {
        public static ESexe GenererAleatoirement()
        {
            ESexe sexe = new ESexe();

            List<ESexe> liste = new List<ESexe>();

            foreach (ESexe s in Enum.GetValues(typeof(ESexe)))
            {
                liste.Add(s);
            }

            sexe = liste[new Random().Next(liste.Count)];

            return sexe;
        }
    }
}
