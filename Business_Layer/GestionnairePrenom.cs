﻿using Data_Access_Layer;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business_Layer
{
    public static class GestionnairePrenom
    {
        /// <summary>
        /// Ajoute une entrée dans Table_Nom
        /// </summary>
        /// <param name="indice">Indice de l'entrée créé (0 si échec)</param>
        /// <param name="proposition">Nom de la nouvelle entrée</param>
        /// <param name="race">Race de la nouvelle entrée</param>
        /// <param name="sexe">Sexe de la nouvelle entrée</param>
        public static bool Ajouter(out long indice, string proposition, ERace race, ESexe sexe)
        {
            return AssistantSqlPrenom.Ajouter(out indice, proposition, race, sexe);
        }

        /// <summary>
        /// Supprime une entrée de Table_Noms
        /// </summary>
        /// <param name="indice">Indice de l'entrée à retirer</param>
        /// <returns>True si l'entrée a bien été supprimée, false dans le cas contraire</returns>
        public static bool Supprimer(long indice)
        {
            bool resultat = false;

            if (Existe(indice))
            {
                AssistantSqlPrenom.Supprimer(indice);
                resultat = true;
            }

            return resultat;
        }

        /// <summary>
        /// Teste l'existence d'une entrée dans Table_Personnage
        /// </summary>
        /// <param name="indice">Indice de l'entrée à trouver</param>
        /// <returns>True si l'entrée existe, false dans le cas contraire</returns>
        public static bool Existe(long indice)
        {
            return AssistantSqlPrenom.Existe(indice);
        }

        /// <summary>
        /// Trouve une entrée dans Table_Nom
        /// </summary>
        /// <param name="indice">Indice de retour (o si échec)</param>
        /// <param name="proposition">Nom recherché</param>
        /// <param name="race">Race recherchée</param>
        /// <param name="sexe">Sexe recherchée</param>
        /// <returns></returns>
        public static bool Trouver(out long indice, string proposition, ERace race, ESexe sexe)
        {
            return AssistantSqlPrenom.Trouver(out indice, proposition, race, sexe);
        }

        /// <summary>
        /// Modifie une entrée de la Table_Personnage
        /// </summary>
        /// <param name="indiceAModifier">Indice de l'entrée à modifier</param>
        /// <param name="proposition">Nouvelle proposition</param>
        /// <param name="race">Nouvelle race</param>
        /// <param name="sexe">Nouveau sexe</param>
        /// <returns></returns>
        public static bool Modifier(long indiceAModifier, string proposition, ERace race, ESexe sexe)
        {
            bool resultat;

            if (AssistantSqlPrenom.Existe(indiceAModifier))
            {
                resultat = AssistantSqlPrenom.Modifier(indiceAModifier, proposition, race, sexe);
            }
            else
            {
                resultat = false;
            }

            return resultat;
        }

        /// <summary>
        /// Extrait un tableau de Table_Nom selon plusieurs critères
        /// </summary>
        /// <param name="proposition">Proposition recherchée</param>
        /// <param name="race">Race recherchée</param>
        /// <param name="sexe">Sexe recherché</param>
        /// <returns></returns>
        public static List<Prenom> ObtenirTableauParRecherche(string proposition, ERace? race, ESexe? sexe)
        {
            return AssistantSqlPrenom.ObtenirTableauRecherche(proposition, race, sexe);
        }

        /// <summary>
        /// Extrait un tableau de toute la table Table_Noms
        /// </summary>
        /// <returns>Liste d'objets de type Nom</returns>
        public static List<Prenom> ObtenirTableauComplet()
        {
            return AssistantSqlPrenom.ObtenirTableauComplet();
        }

        /// <summary>
        /// Renvoie un prénom aléatoire
        /// </summary>
        /// <param name="race">Race demandée</param>
        /// <param name="sexe">Sexe demandé</param>
        /// <returns>Objet de type Prenom</returns>
        public static Prenom GenererAleatoirement(ERace race, ESexe sexe)
        {
            Prenom prenom = new Prenom();

            if (race == ERace.DEMI_ELFE)
            {
                race = DateTime.Now.Millisecond % 2 == 0 ? ERace.HUMAIN : ERace.ELFE;
            }

            List<Prenom> liste = ObtenirTableauParRecherche(null, race, sexe);

            prenom = liste[new Random().Next(liste.Count)];

            return prenom;
        }
        
        /// <summary>
        /// Implémente une liste de noms liés à une race
        /// </summary>
        /// <param name="indice">Indice de retour</param>
        /// <param name="liste">Liste de noms</param>
        /// <param name="race">Race liée</param>
        /// <param name="sexe">Sexe liée</param>
        private static void Implementer(out long indice, List<string> liste, ERace race, ESexe sexe)
        {
            indice = 0;
            foreach (string s in liste)
            {
                AssistantSqlPrenom.Ajouter(out indice, s, race, sexe);
            }

            liste.Clear();
        }

        /// <summary>
        /// Initialise Table_Noms avec les propositions contenues dans la méthode
        /// </summary>
        public static void Initialiser()
        {
            long indice;
            List<string> liste = new List<string>();

            liste.AddRange(new string[] { "Adrik", "Alberich", "Baern", "Barendd", "Brottor", "Bruenor", "Oain", "Oarrak", "Oelg", "Eberk", "Einkil", "Fargrim", "Flint", "Gardain", "Harbek", "Kildrak", "Morgran", "Orsik", "Oskar", "Rangrim", "Rurik", "Taklinn", "Thoradin", "Thorin", "Tordek", "Traubon", "Travok", "Ulfgar", "Veit", "Vondal" });
            Implementer(out indice, liste, ERace.NAIN, ESexe.MALE);

            liste.AddRange(new string[] { "Amber", "Artin", "Audhild", "Bardryn", "Oagnal", "Oiesa", "Eldeth", "Falkrunn", "Finellen", "Gunnloda", "Gurdis", "Helja", "Hlin", "Kathra", "Kristryd", "lide", "Liftrasa", "Mardred", "Riswynn", "Sannl", "Torbera", "Torgga", "Vistra" });
            Implementer(out indice, liste, ERace.NAIN, ESexe.FEMELLE);

            liste.AddRange(new string[] { "Adran", "Aelar", "Aramil", "Arannis", "Aust", "Beiro", "Berrian", "Carrie", "Enialis", "Erdan", "Erevan", "Galinndan", "Hadarai", "Heian", "Himo", "Immeral", "Ivellios", "Laucian", "Mindartis", "Paelias", "Peren", "Quarion", "Riardon", "Rolen", "Soveliss", "Thamior", "Tharivol", "Theren", "Varis" });
            Implementer(out indice, liste, ERace.ELFE, ESexe.MALE);

            liste.AddRange(new string[] { "Adrie", "Althaea", "Anastrianna", "Andraste", "Antinua", "Bethrynna", "Birel", "Caelynn", "Orusilia", "Enna", "Felosial", "lelenia", "Jelenneth", "Keyleth", "Leshanna", "Lia", "Meriele", "Mialee", "Naivara", "Quelenna", "Quillathe", "Sariel", "Shanairra", "Shava", "Silaqui", "Theirastra", "Thia", "Vadania", "Valanthe", "Xanaphia" });
            Implementer(out indice, liste, ERace.ELFE, ESexe.FEMELLE);

            liste.AddRange(new string[] { "Alton", "Ander", "Cade", "Corrin", "Eldon", "Errich", "Finnan", "Garret", "Lindal", "Lyle", "Merric", "Milo", "Osborn", "Perrin", "Reed", "Roscoe", "Wellby" });
            Implementer(out indice, liste, ERace.HALFELIN, ESexe.MALE);

            liste.AddRange(new string[] { "Andry", "Bree", "Callie", "Cora", "Euphemia", "Jillian", "Kithri", "Lavinia", "Lidda", "Merla", "Nedda", "Paela", "Portia", "Seraphina", "Shaena", "Trym", "Vani", "Verna" });
            Implementer(out indice, liste, ERace.HALFELIN, ESexe.FEMELLE);

            liste.AddRange(new string[] { "Aseir", "Bardeid", "Haseid", "Khemed", "Mehmen", "Sudeiman", "Zasheir", "Darvin", "Dorn", "Evendur", "Gorstag", "Grim", "Helm", "Malark", "Morn", "Randal", "Stedd", "Bar", "Fadei", "Glar", "Grigor", "Igan", "Ivor", "Kosef", "Mival", "Orei", "Pavel", "Sergor", "Ander", "Blath", "Bran", "Frath", "Geth", "Lander", "Luth", "Malcer", "Stor", "Taman", "Urth", "Aoth", "Bareris", "Ehput-Ki", "Kethoth", "Mumed", "Ramas", "So-Kehur", "Thazar-De", "Urhur", "Borivik", "Faurgar", "Jandar", "Kanithar", "Madislak", "Ralmevik", "Shaumar", "Vladislak", "An", "Chen", "Chi", "Fai", "Jiang", "Jun", "Lian", "Long", "Meng", "On", "Shan", "Shui", "Wen", "Anton", "Diero", "Marcon", "Pieron", "Rimardo", "Romero", "Salazar", "Umbero" });
            Implementer(out indice, liste, ERace.HUMAIN, ESexe.MALE);

            liste.AddRange(new string[] { "Alala", "Ceidil", "Hama", "Jasmal", "Meilil", "Seipora", "Yasheira", "Zasheida", "Arveene", "Esvele", "Jhessail", "Kerri", "Lureene", "Miri", "Rowan", "Shandri", "Tessele", "Alethra", "Kara", "Katernin", "Mara", "Natali", "Olma", "Tana", "Zora", "Amafrey", "Betha", "Cefrey", "Kethra", "Mara", "Olga", "Silifrey", "Westra", "Arizima", "Chathi", "Nephis", "Nulara", "Murithi", "Sefris", "Thola", "Umara", "Zolis", "Fyevarra", "Hulmarra", "Immith", "Imzel", "Navarra", "Shevarra", "Tammith", "Yuldra", "Bai", "Chao", "Jia", "Lei", "Mei", "Qiao", "Shui", "Tai", "Balama", "Dona", "Faila", "Jalana", "Luisa", "Marta", "Quara", "Selise", "Vonda" });
            Implementer(out indice, liste, ERace.HUMAIN, ESexe.FEMELLE);

            liste.AddRange(new string[] { "Arjhan", "Balasar", "Bharash", "Donaar", "Ghesh", "Heskan", "Kriv", "Medrash", "Mehen", "Nadarr", "Pandjed", "Patrin", "Rhogar", "Shamash", "Shedinn", "Tarhun", "Torinn" });
            Implementer(out indice, liste, ERace.DRAKEIDE, ESexe.MALE);

            liste.AddRange(new string[] { "Akra", "Biri", "Daar", "Farideh", "Harann", "HaviJar", "Jheri", "Kava", "Korinn", "Mishann", "NaJa", "Perra", "Raiann", "Sora", "Surina", "Thava", "Uadjit" });
            Implementer(out indice, liste, ERace.DRAKEIDE, ESexe.FEMELLE);

            liste.AddRange(new string[] { "Alston", "Alvyn", "Boddynock", "Brocc", "Burgell", "Dimble", "Eldon", "Erky", "Fonkin", "Frug", "Gerbo", "Gimble", "Glim", "Jebeddo", "Kellen", "Namfoodle", "Orryn", "Roondar", "Seebo", "Sindri", "Warryn", "Wrenn", "Zook" });
            Implementer(out indice, liste, ERace.GNOME, ESexe.MALE);

            liste.AddRange(new string[] { "Bimpnollin", "Breena", "Caramip", "Carlin", "Donella", "Duvamil", "ElIa", "ElIyjobell", "ElIywick", "Lilli", "Loopmottin", "Lorilla", "Mardnab", "Nissa", "Nyx", "Oda", "Orla", "Roywyn", "Shamil", "Tana", "Waywocket", "Zanna" });
            Implementer(out indice, liste, ERace.GNOME, ESexe.FEMELLE);

            liste.AddRange(new string[] { "Deneh", "Feng", "Gell", "Henk", "Holg", "Imsh", "Kelh", "Krusk", "Mhurren", "Ront", "Shump", "Thokk" });
            Implementer(out indice, liste, ERace.DEMI_ORC, ESexe.MALE);

            liste.AddRange(new string[] { "Baggi", "Emen", "Engong", "Kansif", "Myev", "Neega", "Ovak", "Ownka", "Shaulha", "Sulha", "Vola", "Volen", "Yevelda" });
            Implementer(out indice, liste, ERace.DEMI_ORC, ESexe.FEMELLE);

            liste.AddRange(new string[] { "Akmenos", "Amnon", "Barakas", "Damakos", "Ekemon", "Lados", "Kairon", "Leucis", "Melech", "Mordai", "Morthos", "Pelaios", "Skamos", "Therai" });
            Implementer(out indice, liste, ERace.TIEFELIN, ESexe.MALE);

            liste.AddRange(new string[] { "Akta", "Anakis", "Bryseis", "Criella", "Damaia", "Ea", "Kallista", "Lerissa", "Makaria", "Nemeia", "Orianna", "Phelaia", "Rieta" });
            Implementer(out indice, liste, ERace.TIEFELIN, ESexe.FEMELLE);
        }
    }
}
