﻿using Data_Access_Layer;
using Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business_Layer
{
    public static class GestionnaireMetier
    {
        /// <summary>
        /// Ajoute une entrée à Table_Metiers
        /// </summary>
        /// <param name="intitule">Nom de l'entrée</param>
        /// <param name="ageMinimum">Age minimum requis pour cette entrée</param>
        /// <returns></returns>
        public static bool Ajouter(EMetier intitule, int ageMinimum)
        {
            return AssistantSqlMetier.Ajouter(intitule, ageMinimum);
        }

        /// <summary>
        /// Vérifie si une entrée existe
        /// </summary>
        /// <param name="intitule">Nom de l'entrée à vérifier</param>
        /// <returns>True si l'entrée existe, false dans le cas contraire</returns>
        public static bool Existe(EMetier intitule)
        {
            return AssistantSqlMetier.Existe(intitule);
        }

        /// <summary>
        /// Supprime une entrée de Table_Metiers
        /// </summary>
        /// <param name="intitule">Entrée à supprimer</param>
        /// <returns>True si l'entrée a bien été supprimée, false dans le cas contraire</returns>
        public static bool Supprimer(EMetier intitule)
        {
            bool resultat = false;

            if (Existe(intitule))
            {
                AssistantSqlMetier.Supprimer(intitule);
                resultat = true;
            }

            return resultat;
        }

        /// <summary>
        /// Modifie une entrée de la Table_Metiers
        /// </summary>
        /// <param name="intitule"></param>
        /// <param name="ageMinimum"></param>
        /// <returns></returns>
        public static bool Modifier(EMetier intitule, int ageMinimum)
        {
            bool resultat;

            if (AssistantSqlMetier.Existe(intitule))
            {
                resultat = AssistantSqlMetier.Modifier(intitule, ageMinimum);
            }
            else
            {
                resultat = false;
            }

            return resultat;
        }

        public static Metier GenererAleatoirement(int age)
        {
            Metier metier = new Metier();

            List<Metier> liste = ObtenirTableauParRecherche(null, age);

            metier = liste[new Random().Next(liste.Count)];

            return metier;
        }

        /// <summary>
        /// Extrait un tableau de Table_Metiers selon plusieurs critères
        /// </summary>
        /// <param name="intitule">Nom de l'entrée recherchée (nullable)</param>
        /// <param name="ageMinimum">Age du personnage (nullable)</param>
        /// <returns>Une liste d'objets de type Metier</returns>
        public static List<Metier> ObtenirTableauParRecherche(EMetier? intitule, int? ageMinimum)
        {
            return AssistantSqlMetier.ObtenirTableauRecherche(intitule, ageMinimum);
        }

        /// <summary>
        /// Extrait un tableau complet de Table_Metiers
        /// </summary>
        /// <returns>Liste d'objets de type Metier</returns>
        public static List<Metier> ObtenirTableauComplet()
        {
            return AssistantSqlMetier.ObtenirTableauComplet();
        }

        /// <summary>
        /// Initialise Table_Metier avec la liste de l'énumération EMetier (age minimum par défaut : 15)
        /// </summary>
        public static void Initialiser()
        {
            foreach (EMetier metier in Enum.GetValues(typeof(EMetier)))
            {
                AssistantSqlMetier.Ajouter(metier, 15);
                AssistantSqlMetier.Modifier(EMetier.AUBERGISTE, 25);
                AssistantSqlMetier.Modifier(EMetier.COMMERCANT, 20);
                AssistantSqlMetier.Modifier(EMetier.COMMIS, 12);
                AssistantSqlMetier.Modifier(EMetier.DIRIGEANT, 30);
                AssistantSqlMetier.Modifier(EMetier.ERUDIT, 30);
                AssistantSqlMetier.Modifier(EMetier.PRETRE, 22);
                AssistantSqlMetier.Modifier(EMetier.SANS, 0);
                AssistantSqlMetier.Modifier(EMetier.TAVERNIER, 25);
            }
        }
    }
}
