﻿using Data_Access_Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Business_Layer
{
    public static class GestionnaireBDD
    {
        // Méthodes
        /// <summary>
        /// Teste la connexion au serveur avec changement de nom en cas d'échec puis vérifie sur la base de données existe
        /// </summary>
        /// <returns>True si la base existe, false dans l'autre cas</returns>
        public static bool EstBDDExiste()
        {
            AssistantSql.TesterServeur(AssistantSql.Serveur);

            return AssistantSql.VerifierExistenceBDD();
        }

        /// <summary>
        /// Crée la base de données
        /// </summary>
        public static void CreationBDD()
        {
            AssistantSql.CreerBDD();
        }

        /// <summary>
        /// Vérifie l'existence de chaque table et, en l'absence, crée la table
        /// </summary>
        public static void CreationTables()
        {
            if (!AssistantSql.VerifierExistenceTable(AssistantSql.Table_Personnages))
            {
                AssistantSql.CreerTablePersonnages();
            }

            if (!AssistantSql.VerifierExistenceTable(AssistantSql.Table_Metiers))
            {
                AssistantSql.CreerTableMetiers();
                GestionnaireMetier.Initialiser();
            }

            if (!AssistantSql.VerifierExistenceTable(AssistantSql.Table_Noms))
            {
                AssistantSql.CreerTableNoms();
                GestionnaireNom.Initialiser();
            }

            if (!AssistantSql.VerifierExistenceTable(AssistantSql.Table_Prenoms))
            {
                AssistantSql.CreerTablePrenoms();
                GestionnairePrenom.Initialiser();
            }

            if (!AssistantSql.VerifierExistenceTable(AssistantSql.Table_Communautes))
            {
                AssistantSql.CreerTableCommunautes();
            }

            if (!AssistantSql.VerifierExistenceTable(AssistantSql.Table_Occupations))
            {
                AssistantSql.CreerTableOccupations();
            }
        }

        /// <summary>
        /// Supprime la base de donnnées
        /// </summary>
        public static void SuppressionBDD()
        {
            AssistantSql.SupprimerBDD();
        }
    }
}
