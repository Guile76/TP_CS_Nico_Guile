﻿using Data_Access_Layer;
using Model;
using System.Collections.Generic;

namespace Business_Layer
{
    public static class GestionnairePersonnage
    {
        /// <summary>
        /// Ajoute une entrée dans Table_Personnage
        /// </summary>
        /// <param name="indice">Indice de l'entrée créé (0 si échec)</param>
        /// <param name="nom">Nom de la nouvelle entrée</param>
        /// <param name="prenom">Prénom de la nouvelle entrée</param>
        /// <param name="age">Age de la nouvelle entrée</param>
        /// <param name="race">Race de la nouvelle entrée</param>
        /// <param name="sexe">Sexe de la nouvelle entrée</param>
        /// <param name="metier">Métier de la nouvelle entrée</param>
        /// <returns>True si l'entrée a bien été ajoutée, false dans le cas contraire</returns>
        public static bool Ajouter(out long indice, string nom, string prenom, int age, ERace race, ESexe sexe, EMetier metier)
        {
            return AssistantSqlPersonnage.Ajouter(out indice, nom, prenom, age, race, sexe, metier);
        }

        /// <summary>
        /// Supprime une entrée de Table_Personnage
        /// </summary>
        /// <param name="indice">Indice de l'entrée à retirer</param>
        /// <returns>True si l'entrée a bien été supprimée, false dans le cas contraire</returns>
        public static bool Supprimer(long indice)
        {
            bool resultat = false;

            if (Existe(indice))
            {
                AssistantSqlPersonnage.Supprimer(indice);
                resultat = true;
            }

            return resultat;
        }

        /// <summary>
        /// Teste l'existence d'une entrée dans Table_Personnage
        /// </summary>
        /// <param name="indice">Indice de l'entrée à trouver</param>
        /// <returns>True si l'entrée existe, false dans le cas contraire</returns>
        public static bool Existe(long indice)
        {
            return AssistantSqlPersonnage.Existe(indice);
        }

        /// <summary>
        /// Trouve une entrée dans Table_Personnage
        /// </summary>
        /// <param name="indice">Indice de retour (0 si échec)</param>
        /// <param name="nom">Nom recherché</param>
        /// <param name="prenom">Prénom recherché</param>
        /// <param name="age">Age recherché</param>
        /// <param name="race">Race recherchée</param>
        /// <param name="sexe">Sexe recherché</param>
        /// <param name="metier">Métier recherché</param>
        /// <returns></returns>
        public static bool Trouver(out long indice, string nom, string prenom, int age, ERace race, ESexe sexe, EMetier metier)
        {
            return AssistantSqlPersonnage.Trouver(out indice, nom, prenom, age, race, sexe, metier);
        }

        /// <summary>
        /// Modifie une entrée de la Table_Personnage.
        /// </summary>
        /// <param name="IndiceAModifier">Indice de l'entrée à modifier</param>
        /// <param name="nom">Nouveau nom</param>
        /// <param name="prenom">Nouveau prénom</param>
        /// <param name="age">Nouvel age</param>
        /// <param name="race">Nouvelle race</param>
        /// <param name="sexe">Nouveau sexe</param>
        /// <param name="metier">Nouveau métier</param>
        /// <returns>True si la modification a bien été effectuée, false dans le cas contraire</returns>
        public static bool Modifier(long IndiceAModifier, string nom, string prenom, int age, ERace race, ESexe sexe, EMetier metier)
        {
            bool resultat;

            if (AssistantSqlPersonnage.Existe(IndiceAModifier))
            {
                resultat = AssistantSqlPersonnage.Modifier(IndiceAModifier, nom, prenom, age, race, sexe, metier);
            }
            else
            {
                resultat = false;
            }

            return resultat;
        }

        /// <summary>
        /// Extrait un tableau de Table_Personnages selon plusieurs critères
        /// </summary>
        /// <param name="nom">Nom recherché (nullable)</param>
        /// <param name="prenom">Prénom recherché (nullable)</param>
        /// <param name="age">Age recherché (nullable)</param>
        /// <param name="race">Race recherchée (nullable)</param>
        /// <param name="sexe">Sexe recherché (nullable)</param>
        /// <param name="metier">Métier recherché (nullable)</param>
        /// <returns>Objet de type DataTable</returns>
        public static List<Personnage> ObtenirTableauParRecherche(string nom, string prenom, int? age, ERace? race, ESexe? sexe, EMetier? metier)
        {
            return AssistantSqlPersonnage.ObtenirTableauRecherche(nom, prenom, age, race, sexe, metier);
        }

        /// <summary>
        /// Extrait un tableau de toute la table Table_Personnages
        /// </summary>
        /// <returns>Liste d'objets de type Personnage</returns>
        public static List<Personnage> ObtenirTableauComplet()
        {
            return AssistantSqlPersonnage.ObtenirTableauComplet();
        }
    }
}
