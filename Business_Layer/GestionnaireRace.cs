﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business_Layer
{
    public static class GestionnaireRace
    {
        public static ERace GenererAleatoirement()
        {
            ERace race = new ERace();

            List<ERace> liste = new List<ERace>();

            foreach (ERace r in Enum.GetValues(typeof(ERace)))
            {
                liste.Add(r);
            }

            race = liste[new Random().Next(liste.Count)];

            return race;
        }
    }
}
