﻿using Data_Access_Layer;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business_Layer
{
    public static class GestionnaireNom
    {
        /// <summary>
        /// Ajoute une entrée dans Table_Nom
        /// </summary>
        /// <param name="indice">Indice de l'entrée créé (0 si échec)</param>
        /// <param name="proposition">Nom de la nouvelle entrée</param>
        /// <param name="race">Race de la nouvelle entrée</param>
        public static bool Ajouter(out long indice, string proposition, ERace race)
        {
            return AssistantSqlNom.Ajouter(out indice, proposition, race);
        }

        /// <summary>
        /// Supprime une entrée de Table_Noms
        /// </summary>
        /// <param name="indice">Indice de l'entrée à retirer</param>
        /// <returns>True si l'entrée a bien été supprimée, false dans le cas contraire</returns>
        public static bool Supprimer(long indice)
        {
            bool resultat = false;

            if (Existe(indice))
            {
                AssistantSqlNom.Supprimer(indice);
                resultat = true;
            }

            return resultat;
        }

        /// <summary>
        /// Teste l'existence d'une entrée dans Table_Personnage
        /// </summary>
        /// <param name="indice">Indice de l'entrée à trouver</param>
        /// <returns>True si l'entrée existe, false dans le cas contraire</returns>
        public static bool Existe(long indice)
        {
            return AssistantSqlNom.Existe(indice);
        }

        /// <summary>
        /// Trouve une entrée dans Table_Nom
        /// </summary>
        /// <param name="indice">Indice de retour (o si échec)</param>
        /// <param name="proposition">Nom recherché</param>
        /// <param name="race">Race recherchée</param>
        /// <returns></returns>
        public static bool Trouver(out long indice, string proposition, ERace race)
        {
            return AssistantSqlNom.Trouver(out indice, proposition, race);
        }

        /// <summary>
        /// Modifie une entrée de la Table_Personnage
        /// </summary>
        /// <param name="indiceAModifier">Indice de l'entrée à modifier</param>
        /// <param name="proposition">Nouvelle proposition</param>
        /// <param name="race">Nouvelle race</param>
        /// <returns></returns>
        public static bool Modifier(long indiceAModifier, string proposition, ERace race)
        {
            bool resultat;

            if (AssistantSqlNom.Existe(indiceAModifier))
            {
                resultat = AssistantSqlNom.Modifier(indiceAModifier, proposition, race);
            }
            else
            {
                resultat = false;
            }

            return resultat;
        }

        /// <summary>
        /// Extrait un tableau de Table_Nom selon plusieurs critères
        /// </summary>
        /// <param name="proposition">Proposition recherchée</param>
        /// <param name="race">Race recherchée</param>
        /// <returns></returns>
        public static List<Nom> ObtenirTableauParRecherche(string proposition, ERace? race)
        {
            return AssistantSqlNom.ObtenirTableauRecherche(proposition, race);
        }

        /// <summary>
        /// Extrait un tableau de toute la table Table_Noms
        /// </summary>
        /// <returns>Liste d'objets de type Nom</returns>
        public static List<Nom> ObtenirTableauComplet()
        {
            return AssistantSqlNom.ObtenirTableauComplet();
        }

        /// <summary>
        /// Génère aléatoirement un nom en fonction de la race
        /// </summary>
        /// <param name="race">Race souhaitée (un Demi-Orc n'aura pas de proposition)</param>
        /// <returns>Objet de type Nom</returns>
        public static Nom GenererAleatoirement(ERace race)
        {
            Nom nom = new Nom("", ERace.DEMI_ORC);
            if (race != ERace.DEMI_ORC)
            {
                if (race == ERace.DEMI_ELFE)
                {
                    race = ERace.HUMAIN;
                }

                List<Nom> liste = ObtenirTableauParRecherche(null, race);

                nom = liste[new Random().Next(liste.Count)];
            }

            return nom;
        }

        /// <summary>
        /// Implémente une liste de noms liés à une race
        /// </summary>
        /// <param name="indice">Indice de retour</param>
        /// <param name="liste">Liste de noms</param>
        /// <param name="race">Race liée</param>
        private static void Implementer(out long indice, List<string> liste, ERace race)
        {
            indice = 0;
            foreach (string s in liste)
            {
                AssistantSqlNom.Ajouter(out indice, s, race);
            }

            liste.Clear();
        }

        /// <summary>
        /// Initialise Table_Noms avec les propositions contenues dans la méthode
        /// </summary>
        public static void Initialiser()
        {
            long indice;
            List<string> liste = new List<string>();

            liste.AddRange(new string[] {"Balderk", "Battlehammer", "Brawnanvil", "Oankil", "Fireforge", "Frostbeard", "Gorunn", "Holderhek", "Ironfist", "Loderr", "Lutgehr", "Rumnaheim", "Strakeln", "Torunn", "Ungart" });
            Implementer(out indice, liste, ERace.NAIN);

            liste.AddRange(new string[] { "Amakiir (Gemflower)", "Amastacia (Starflower)", "Galanodel (Moonwhisper)", "Holimion (Oiamonddew)", "IIphelkiir (Gemblossom)", "Liadon (Silverfrond)", "Meliamne (Oakenheel)", "Nailo (Nightbreeze)", "Siannodel (Moonbrook)", "Xiloscient(Goldpetal)" });
            Implementer(out indice, liste, ERace.ELFE);

            liste.AddRange(new string[] { "Brushgather", "Goodbarrel", "Greenbottle", "Highhill", "Hilltopple", "Leagallow", "Tealeaf", "Thorngage", "Tosscobble", "Underbough" });
            Implementer(out indice, liste, ERace.HALFELIN);

            liste.AddRange(new string[] { "Basha", "Dumein", "Jassan", "Khalid", "Moslana", "Pashar", "Rein", "Amblecrown", "Buckman", "Dundragon", "Evenwood", "Greycastle", "Tallstag", "Bersk", "Chernin", "Dotsk", "Kulenov", "Marsk", "Nemetsk", "Shemov", "Starag", "Brightwood", "Helder", "Hornraven", "Lackman", "Stormwind", "Windrivver", "Ankhalab", "Anskuld", "Fezim", "Hahpet", "Nathandem", "Sepret", "Uuthrakt", "Chergoba", "Dyernina", "Iltazyara", "Murnyethara", "Stayanoga", "Ulmokina", "Chien", "Huang", "Kao", "Kung", "Lao", "Ling", "Mei", "Pin", "Shin", "Sum", "Tan", "Wan", "Agosto", "Astorio", "Calabra", "Domine", "Falone", "Marivaldi", "Pisacar", "Ramondo" });
            Implementer(out indice, liste, ERace.HUMAIN);

            liste.AddRange(new string[] { "Clethtinthiallor", "Daardendrian", "Delmirev", "Drachedandion", "Fenkenkabradon", "Kepeshkmolik", "Kerrhylon", "Kimbatuul", "Linxakasendalor", "Myastan", "Nemmonis", "Norixius", "Ophinshtalajiir", "Prexijandilin", "Shestendeliath", "Turnuroth", "Verthisathurgiesh", "Yarjerit" });
            Implementer(out indice, liste, ERace.DRAKEIDE);

            liste.AddRange(new string[] { "Beren", "Daergel", "Folkor", "Garrick", "Nackle", "Murnig", "Ningel", "Raulnor", "Scheppen", "Timbers", "Turen" });
            Implementer(out indice, liste, ERace.GNOME);


            liste.AddRange(new string[] { "Art", "Carrion", "Chant", "Creed", "Despair", "Excellence", "Fear", "Glory", "Hope", "Ideal", "Music", "Nowhere", "Open", "Poetry", "Quest", "Random", "Reverence", "Sorrow", "Temerily", "Torment", "Weary" });
            Implementer(out indice, liste, ERace.TIEFELIN);
        }

    }
}
