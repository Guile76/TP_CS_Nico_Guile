﻿using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Data_Access_Layer
{
    public static class AssistantSqlNom
    {
        // Attribut
        private static string nomTable = AssistantSql.Table_Noms;

        /// <summary>
        /// Ajoute une entrée à Table_Noms
        /// </summary>
        /// <param name="indice">Indice de l'entrée créée (0 si échec)</param>
        /// <param name="proposition">Nom de la nouvelle entrée</param>
        /// <param name="race">Race de la nouvelle entrée</param>
        /// <returns></returns>
        public static bool Ajouter(out long indice, string proposition, ERace race)
        {
            bool resultat = false;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"INSERT INTO {0} (proposition, race) OUTPUT INSERTED.id values (@proposition, @race)", nomTable);

                        commandeBDD.Parameters.AddWithValue("@proposition", proposition);
                        commandeBDD.Parameters.AddWithValue("@race", race);

                        indice = (long)commandeBDD.ExecuteScalar();

                        resultat = true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
                indice = 0;
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Supprimer une entrée de Table_Personnages
        /// </summary>
        /// <param name="indiceProposition">Indice de l'entrée à supprimer</param>
        /// <returns>True si l'opération s'est bien effectuée, false dans le cas contraire</returns>
        public static bool Supprimer(long indiceProposition)
        {
            bool resultat = false;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"DELETE FROM {0} WHERE id = @id", nomTable);

                        commandeBDD.Parameters.AddWithValue("@id", indiceProposition);

                        commandeBDD.ExecuteNonQuery();

                        resultat = true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Vérifie si une entrée existe dans Table_Personnage
        /// </summary>
        /// <param name="indiceProposition">Indice de l'entrée à trouver</param>
        /// <returns>True si l'entrée est trouvée, false dans le cas contraire</returns>
        public static bool Existe(long indiceProposition)
        {
            bool resultat = false;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"SELECT * FROM {0} WHERE id = @id", nomTable);

                        commandeBDD.Parameters.AddWithValue("@id", indiceProposition);

                        long compte = Convert.ToInt64(commandeBDD.ExecuteScalar());

                        if (compte == indiceProposition)
                        {
                            resultat = true;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// /// Trouve une entrée à partir de ses attributs
        /// </summary>
        /// <param name="indice">Indice de retour de l'entrée trouvée</param>
        /// <param name="proposition">Nom à trouver</param>
        /// <param name="race">Race à trouver</param>
        /// <returns>True si l'entrée est trouvée, false dans le cas contraire</returns>
        public static bool Trouver(out long indice, string proposition, ERace race)
        {
            bool resultat = false;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"SELECT * FROM {0} WHERE proposition = @proposition AND race = @race", nomTable);

                        commandeBDD.Parameters.AddWithValue("@proposition", proposition);
                        commandeBDD.Parameters.AddWithValue("@race", (int)race);

                        indice = Convert.ToInt64(commandeBDD.ExecuteScalar());
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
                indice = 0;
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            if (indice > 0)
            {
                resultat = true;
            }

            return resultat;
        }

        /// <summary>
        /// Modifie l'entrée sélectionnée
        /// </summary>
        /// <param name="indiceEntreeAModifier">Indice de la proposition à modifier</param>
        /// <param name="proposition">Nouvelle proposition</param>
        /// <param name="race">Nouvelle race</param>
        /// <returns>True si l'entrée est bien modifiée, false dans le cas contraire</returns>
        public static bool Modifier(long indiceEntreeAModifier, string proposition, ERace race)
        {
            bool resultat = false;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        if (Existe(indiceEntreeAModifier))
                        {
                            commandeBDD.CommandText = string.Format(@"UPDATE {0} SET proposition = @proposition, race = @race where id = @id", nomTable);

                            commandeBDD.Parameters.AddWithValue("@proposition", proposition);
                            commandeBDD.Parameters.AddWithValue("@race", (int)race);
                            commandeBDD.Parameters.AddWithValue("@id", indiceEntreeAModifier);

                            commandeBDD.ExecuteNonQuery();

                            resultat = true;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Récupère une entrée dans Table_Nom
        /// </summary>
        /// <param name="indiceEntree">Indice recherché</param>
        /// <returns>Objet de type Nom</returns>
        public static Nom Obtenir(long indiceEntree)
        {
            Nom nom = null;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"SELECT id, proposition, race FROM {0} WHERE id = @id", nomTable);

                        commandeBDD.Parameters.AddWithValue("@id", indiceEntree);

                        using (SqlDataReader lecteur = commandeBDD.ExecuteReader())
                        {
                            while (lecteur.Read())
                            {
                                nom = new Nom();
                                nom.Id = lecteur.GetInt64(0);
                                nom.Proposition = lecteur.GetString(1);
                                nom.Race = (ERace)lecteur.GetInt32(2);
                            }
                        }

                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return nom;
        }

        /// <summary>
        /// Extrait un tableau de Table_Noms selon multi-critères
        /// </summary>
        /// <param name="proposition">Nom recherché</param>
        /// <param name="race">Race recherchée</param>
        /// <returns></returns>
        public static List<Nom> ObtenirTableauRecherche(string proposition, ERace? race)
        {
            // Variables
            List<Nom> liste = new List<Nom>();
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);
            SqlConnection connexion = new SqlConnection(connecterABDD);

            // Connexion
            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        string texteCommande = string.Format(@"SELECT * FROM {0} WHERE id IS NOT NULL", nomTable);

                        if (proposition != null)
                        {
                            texteCommande += " AND proposition = @proposition";
                        }

                        if (race != null)
                        {
                            texteCommande += " AND race = @race";
                        }

                        commandeBDD.CommandText = texteCommande;

                        if (proposition != null)
                        {
                            commandeBDD.Parameters.AddWithValue("@proposition", proposition);
                        }

                        if (race != null)
                        {
                            commandeBDD.Parameters.AddWithValue("@race", race);
                        }

                        DataTable table = new DataTable();
                        DataSet dataSet = new DataSet();
                        SqlDataAdapter adaptateur = new SqlDataAdapter(commandeBDD);
                        adaptateur.Fill(dataSet);

                        table = dataSet.Tables[0];

                        DataRowCollection rangs = table.Rows;

                        foreach (DataRow data in rangs)
                        {
                            Nom nom = new Nom();
                            nom.Id = Convert.ToInt64(data["id"]);
                            nom.Proposition = Convert.ToString(data["proposition"]);
                            nom.Race = (ERace)Convert.ToInt32(data["race"]);

                            liste.Add(nom);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return liste;
        }

        /// <summary>
        /// Extrait un tableau de toute la table Table_Personnage
        /// </summary>
        /// <returns>Une liste d'objets de type Nom</returns>
        public static List<Nom> ObtenirTableauComplet()
        {
            // Variables
            List<Nom> liste = new List<Nom>();
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);
            SqlConnection connexion = new SqlConnection(connecterABDD);

            // Connexion
            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"SELECT * FROM {0}", nomTable);

                        DataTable table = new DataTable();
                        DataSet dataSet = new DataSet();
                        SqlDataAdapter adaptateur = new SqlDataAdapter(commandeBDD);

                        adaptateur.Fill(dataSet);

                        table = dataSet.Tables[0];

                        DataRowCollection rangs = table.Rows;

                        foreach (DataRow data in rangs)
                        {
                            Nom nom = new Nom();
                            nom.Id = Convert.ToInt64(data["id"]);
                            nom.Proposition = Convert.ToString(data["nom"]);
                            nom.Race = (ERace)Convert.ToInt32(data["race"]);

                            liste.Add(nom);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return liste;
        }
    }
}
