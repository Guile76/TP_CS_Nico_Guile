﻿using System;
using System.Data.SqlClient;
using System.Windows;

namespace Data_Access_Layer
{
    public static partial class AssistantSql
    {
        // Attributs
        private static string table_personnages = "personnages";
        private static string table_metiers = "metiers";
        private static string table_prenoms = "prenoms";
        private static string table_noms = "noms";
        private static string table_communautes = "communautes";
        private static string table_occupations = "occupations";

        // Propriétés
        public static string Table_Personnages { get { return table_personnages; } }
        public static string Table_Metiers { get { return table_metiers; } }
        public static string Table_Prenoms { get { return table_prenoms; } }
        public static string Table_Noms { get { return table_noms; } }
        public static string Table_Communautes { get { return table_communautes; } }
        public static string Table_Occupations { get { return table_occupations; } }

        // Méthodes
        /// <summary>
        /// Envoie une requête SQL à la base de donnée NOM_BDD
        /// </summary>
        /// <param name="commande">Commande SQL</param>
        /// <returns>True si la commande a bien été passée, false dans le cas contraire</returns>
        public static bool PasserCommandeBDD(string commande)
        {
            bool resultat = false;
            string commandeComplete = string.Format("USE {0} ", nom_bdd);
            commandeComplete += commande;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", serveur, nom_bdd);

        SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = commandeComplete;
                        commandeBDD.ExecuteNonQuery();
                        resultat = true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Vérifie l'existence d'une table spécifique dans BDD
        /// </summary>
        /// <param name="nomTable">Nom de la table recherchée</param>
        /// <returns>True si la table est trouvée, false dans le case contraire</returns>
        public static bool VerifierExistenceTable(string nomTable)
        {
            string str = string.Format("SELECT OBJECT_ID('{0}')", nomTable);
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", serveur, nom_bdd);
            bool resultat = false;

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand maCommande = connexion.CreateCommand())
                    {
                        maCommande.CommandText = str;
                        resultat = (maCommande.ExecuteScalar() != DBNull.Value);
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Créer la table TABLE_PERSONNAGES dans la BDD
        /// </summary>
        /// <returns>True si la commande s'est bien exécutée, false dans le cas contraire</returns>
        public static bool CreerTablePersonnages()
        {
            string commande = string.Format("CREATE TABLE {0} (id BIGINT IDENTITY (1,1) NOT NULL, nom VARCHAR (40) NOT NULL, prenom VARCHAR (40) NOT NULL, age INT NOT NULL, race INT NOT NULL, sexe INT NOT NULL, metier INT NOT NULL, CONSTRAINT PK_{0} PRIMARY KEY CLUSTERED (id), CONSTRAINT personnages_age CHECK(age >= 0))", table_personnages);

            return PasserCommandeBDD(commande);
        }

        /// <summary>
        /// Crée la table TABLE_METIERS dans la BDD
        /// </summary>
        /// <returns>True si la commande s'est bien exécutée, false dans le cas contraire</returns>
        public static bool CreerTableMetiers()
        {
            string commande = string.Format("CREATE TABLE {0} ( intitule INT NOT NULL, age_minimum INT NOT NULL, CONSTRAINT PK_{0} PRIMARY KEY CLUSTERED (intitule), CONSTRAINT metiers_age_minimum CHECK(age_minimum >= 0))", table_metiers);

            return PasserCommandeBDD(commande);
        }

        /// <summary>
        /// Crée la table TABLE_PRENOMS dans la BDD
        /// </summary>
        /// <returns>True si la commande s'est bien exécutée, false dans le cas contraire</returns>
        public static bool CreerTablePrenoms()
        {
            string commande = string.Format("CREATE TABLE {0} (id BIGINT IDENTITY (1,1) NOT NULL, proposition VARCHAR (40) NOT NULL, race INT NOT NULL, sexe INT NOT NULL, CONSTRAINT PK_{0} PRIMARY KEY CLUSTERED (id))", table_prenoms);

            return PasserCommandeBDD(commande);
        }

        /// <summary>
        /// Crée la table TABLE_NOMS dans la BDD
        /// </summary>
        /// <returns>True si la commande s'est bien exécutée, false dans le cas contraire</returns>
        public static bool CreerTableNoms()
        {
            string commande = string.Format("CREATE TABLE {0}(id BIGINT IDENTITY (1,1) NOT NULL, proposition VARCHAR (40) NOT NULL, race INT NOT NULL, CONSTRAINT PK_{0} PRIMARY KEY CLUSTERED (id))", table_noms);

            return PasserCommandeBDD(commande);
        }

        /// <summary>
        /// Crée la table TABLE_COMMUNAUTES dans la BDD
        /// </summary>
        /// <returns>True si la commande s'est bien exécutée, false dans le cas contraire</returns>
        public static bool CreerTableCommunautes()
        {
            string commande = string.Format("CREATE TABLE {0} ( id BIGINT IDENTITY (1,1) NOT NULL, nom VARCHAR (40) NOT NULL, taille INT NOT NULL, CONSTRAINT PK_{0} PRIMARY KEY CLUSTERED (id), CONSTRAINT communautes_taille CHECK(taille > 0))", table_communautes);

            return PasserCommandeBDD(commande);
        }

        /// <summary>
        /// Crée la table TABLE_OCCUPATIONS dans la BDD
        /// </summary>
        /// <returns>True si la commande s'est bien exécutée, false dans le cas contraire</returns>
        public static bool CreerTableOccupations()
        {
            string commande = string.Format("CREATE TABLE {0} ( id_personnage BIGINT NOT NULL, id_communaute BIGINT NOT NULL, PRIMARY KEY (id_personnage, id_communaute), CONSTRAINT FK_{0}_personnage FOREIGN KEY (id_personnage) REFERENCES {1} (id), CONSTRAINT FK_{0}_communaute FOREIGN KEY (id_communaute) REFERENCES {2} (id))", table_occupations, table_personnages, table_communautes);

            return PasserCommandeBDD(commande);
        }
    }
}
