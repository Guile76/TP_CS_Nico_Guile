﻿using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows;

namespace Data_Access_Layer
{
    public static class AssistantSqlPersonnage
    {
        // Attribut
        private static string nomTable = AssistantSql.Table_Personnages;

        /// <summary>
        /// Ajoute une entrée à Table_Personnage
        /// </summary>
        /// <param name="indice">Indice de l'entrée créé (0 si échec)</param>
        /// <param name="nom">Nom de la nouvelle entrée</param>
        /// <param name="prenom">Prénom de la nouvelle entrée</param>
        /// <param name="age">Age de la nouvelle entrée</param>
        /// <param name="race">Race de la nouvelle entrée</param>
        /// <param name="sexe">Sexe de la nouvelle entrée</param>
        /// <param name="metier">Métier de la nouvelle entrée</param>
        /// <returns>True si l'entrée a bien été ajoutée, false dans le cas contraire</returns>
        public static bool Ajouter(out long indice, string nom, string prenom, int age, ERace race, ESexe sexe, EMetier metier)
        {
            bool resultat = false;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"INSERT INTO {0} (nom, prenom, age, race, sexe, metier) OUTPUT INSERTED.id values (@nom, @prenom, @age, @race,@sexe, @metier)", nomTable);

                        commandeBDD.Parameters.AddWithValue("@nom", nom);
                        commandeBDD.Parameters.AddWithValue("@prenom", prenom);
                        commandeBDD.Parameters.AddWithValue("@age", age);
                        commandeBDD.Parameters.AddWithValue("@race", race);
                        commandeBDD.Parameters.AddWithValue("@sexe", sexe);
                        commandeBDD.Parameters.AddWithValue("@metier", metier);

                        indice = (long)commandeBDD.ExecuteScalar();

                        resultat = true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
                indice = 0;
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Supprimer une entrée de Table_Personnages
        /// </summary>
        /// <param name="indicePersonnage">Indice de l'entrée à supprimer</param>
        /// <returns>True si l'opération s'est bien effectuée, false dans le cas contraire</returns>
        public static bool Supprimer(long indicePersonnage)
        {
            bool resultat = false;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"DELETE FROM {0} WHERE id = @id", nomTable);

                        commandeBDD.Parameters.AddWithValue("@id", indicePersonnage);

                        commandeBDD.ExecuteNonQuery();

                        resultat = true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Vérifie si une entrée existe dans Table_Personnage
        /// </summary>
        /// <param name="indicePersonnage">Indice de l'entrée à trouver</param>
        /// <returns>True si l'entrée est trouvée, false dans le cas contraire</returns>
        public static bool Existe(long indicePersonnage)
        {
            bool resultat = false;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"SELECT * FROM {0} WHERE id = @id", nomTable);

                        commandeBDD.Parameters.AddWithValue("@id", indicePersonnage);

                        long compte = Convert.ToInt64(commandeBDD.ExecuteScalar());

                        if (compte == indicePersonnage)
                        {
                            resultat = true;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Trouve une entrée à partir de ses attributs
        /// </summary>
        /// <param name="indice">Indice de retour de l'entrée trouvée</param>
        /// <param name="nom">Nom à trouver</param>
        /// <param name="prenom">Prénom à trouver</param>
        /// <param name="age">Age à trouver</param>
        /// <param name="race">Race à trouver</param>
        /// <param name="sexe">Sexe à trouver</param>
        /// <param name="metier">Metier à trouver</param>
        /// <returns>True si l'entrée est trouvée, false dans le cas contraire</returns>
        public static bool Trouver(out long indice, string nom, string prenom, int age, ERace race, ESexe sexe, EMetier metier)
        {
            bool resultat = false;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"SELECT * FROM {0} WHERE nom = @nom AND prenom = @prenom AND age = @age AND race = @race AND sexe = @sexe AND metier = @metier", nomTable);

                        commandeBDD.Parameters.AddWithValue("@nom", nom);
                        commandeBDD.Parameters.AddWithValue("@prenom", prenom);
                        commandeBDD.Parameters.AddWithValue("@age", age);
                        commandeBDD.Parameters.AddWithValue("@race", (int)race);
                        commandeBDD.Parameters.AddWithValue("@sexe", (int)sexe);
                        commandeBDD.Parameters.AddWithValue("@metier", (int)metier);

                        indice = Convert.ToInt64(commandeBDD.ExecuteScalar());
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
                indice = 0;
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            if (indice > 0)
            {
                resultat = true;
            }

            return resultat;
        }

        /// <summary>
        /// Modifie l'entrée sélectionnée
        /// </summary>
        /// <param name="indicePersonnageAModifier">Indice du personnage à modifier</param>
        /// <param name="nom">Nouveau nom</param>
        /// <param name="prenom">Nouveau prénom</param>
        /// <param name="age">Nouvel age</param>
        /// <param name="race">Nouvelle race</param>
        /// <param name="sexe">Nouveau sexe</param>
        /// <param name="metier">Nouveau métier</param>
        /// <returns>True si l'entrée est bien modifiée, false dans le cas contraire</returns>
        public static bool Modifier(long indicePersonnageAModifier, string nom, string prenom, int age, ERace race, ESexe sexe, EMetier metier)
        {
            bool resultat = false;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        if (Existe(indicePersonnageAModifier))
                        {
                            commandeBDD.CommandText = string.Format(@"UPDATE {0} SET nom = @nom, prenom = @prenom, age = @age, race = @race, sexe = @sexe, metier = @metier where id = @id", nomTable);

                            commandeBDD.Parameters.AddWithValue("@nom", nom);
                            commandeBDD.Parameters.AddWithValue("@prenom", prenom);
                            commandeBDD.Parameters.AddWithValue("@age", age);
                            commandeBDD.Parameters.AddWithValue("@race", (int)race);
                            commandeBDD.Parameters.AddWithValue("@sexe", (int)sexe);
                            commandeBDD.Parameters.AddWithValue("@metier", (int)metier);
                            commandeBDD.Parameters.AddWithValue("@id", indicePersonnageAModifier);

                            commandeBDD.ExecuteNonQuery();

                            resultat = true;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Récupère une entrée dans Table_Personnage
        /// </summary>
        /// <param name="indicePersonnage">Indice recherché</param>
        /// <returns>Objet de type Personnage</returns>
        public static Personnage Obtenir(long indicePersonnage)
        {
            Personnage personnage = null;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"SELECT id, nom, prenom, age, race, sexe, metier FROM {0} WHERE id = @id", nomTable);

                        commandeBDD.Parameters.AddWithValue("@id", indicePersonnage);

                        using (SqlDataReader lecteur = commandeBDD.ExecuteReader())
                        {
                            while (lecteur.Read())
                            {
                                personnage = new Personnage();
                                personnage.Id = lecteur.GetInt64(0);
                                personnage.Nom = lecteur.GetString(1);
                                personnage.Prenom = lecteur.GetString(2);
                                personnage.Age = lecteur.GetInt32(3);
                                personnage.Race = (ERace)lecteur.GetInt32(4);
                                personnage.Sexe = (ESexe)lecteur.GetInt32(5);
                                personnage.Metier = (EMetier)lecteur.GetInt32(6);
                            }
                        }

                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return personnage;
        }

        /// <summary>
        /// Extrait un tableau de Table_Personnages selon multi-critères
        /// </summary>
        /// <param name="nom">Nom recherché (nullable)</param>
        /// <param name="prenom">Prénom recherché (nullable)</param>
        /// <param name="age">Age recherché (nullable)</param>
        /// <param name="race">Race recherchée (nullable)</param>
        /// <param name="sexe">Sexe recherché (nullable)</param>
        /// <param name="metier">Métier recherché (nullable)</param>
        /// <returns>Objet de type DataTable avec les données récupérées</returns>
        public static List<Personnage> ObtenirTableauRecherche(string nom, string prenom, int? age, ERace? race, ESexe? sexe, EMetier? metier)
        {
            // Variables
            List<Personnage> liste = new List<Personnage>();
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);
            SqlConnection connexion = new SqlConnection(connecterABDD);

            // Connexion
            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        string texteCommande = string.Format(@"SELECT * FROM {0} WHERE id IS NOT NULL", nomTable);

                        if (nom != null)
                        {
                            texteCommande += " AND nom = @nom";
                        }

                        if (prenom != null)
                        {
                            texteCommande += " AND prenom = @prenom";
                        }

                        if (age != null)
                        {
                            texteCommande += " AND age = @age";
                        }

                        if (race != null)
                        {
                            texteCommande += " AND race = @race";
                        }

                        if (sexe != null)
                        {
                            texteCommande += " AND sexe = @sexe";
                        }

                        if (metier != null)
                        {
                            texteCommande += " AND metier = @metier";
                        }

                        commandeBDD.CommandText = texteCommande;

                        if (nom != null)
                        {
                            commandeBDD.Parameters.AddWithValue("@nom", nom);
                        }

                        if (prenom != null)
                        {
                            commandeBDD.Parameters.AddWithValue("@prenom", prenom);
                        }

                        if (age != null)
                        {
                            commandeBDD.Parameters.AddWithValue("@age", age);
                        }

                        if (race != null)
                        {
                            commandeBDD.Parameters.AddWithValue("@race", race);
                        }

                        if (sexe != null)
                        {
                            commandeBDD.Parameters.AddWithValue("@sexe", sexe);
                        }

                        if (metier != null)
                        {
                            commandeBDD.Parameters.AddWithValue("@metier", metier);
                        }

                        DataTable table = new DataTable();
                        DataSet dataSet = new DataSet();
                        SqlDataAdapter adaptateur = new SqlDataAdapter(commandeBDD);
                        adaptateur.Fill(dataSet);

                        table = dataSet.Tables[0];

                        DataRowCollection rangs = table.Rows;

                        foreach (DataRow data in rangs)
                        {
                            Personnage perso = new Personnage();
                            perso.Id = Convert.ToInt64(data["id"]);
                            perso.Nom = Convert.ToString(data["nom"]);
                            perso.Prenom = Convert.ToString(data["prenom"]);
                            perso.Age = Convert.ToInt32(data["age"]);
                            perso.Race = (ERace)Convert.ToInt32(data["race"]);
                            perso.Sexe = (ESexe)Convert.ToInt32(data["sexe"]);
                            perso.Metier = (EMetier)Convert.ToInt32(data["metier"]);

                            liste.Add(perso);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return liste;
        }

        /// <summary>
        /// Extrait un tableau de toute la table Table_Personnage
        /// </summary>
        /// <returns>Une liste d'objets de type Personnage</returns>
        public static List<Personnage> ObtenirTableauComplet()
        {
            // Variables
            List<Personnage> liste = new List<Personnage>();
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);
            SqlConnection connexion = new SqlConnection(connecterABDD);

            // Connexion
            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"SELECT * FROM {0}", nomTable);

                        DataTable table = new DataTable();
                        DataSet dataSet = new DataSet();
                        SqlDataAdapter adaptateur = new SqlDataAdapter(commandeBDD);

                        adaptateur.Fill(dataSet);

                        table = dataSet.Tables[0];

                        DataRowCollection rangs = table.Rows;

                        foreach (DataRow data in rangs)
                        {
                            Personnage perso = new Personnage();
                            perso.Id = Convert.ToInt64(data["id"]);
                            perso.Nom = Convert.ToString(data["nom"]);
                            perso.Prenom = Convert.ToString(data["prenom"]);
                            perso.Age = Convert.ToInt32(data["age"]);
                            perso.Race = (ERace)Convert.ToInt32(data["race"]);
                            perso.Sexe = (ESexe)Convert.ToInt32(data["sexe"]);
                            perso.Metier = (EMetier)Convert.ToInt32(data["metier"]);

                            liste.Add(perso);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return liste;
        }
    }
}
