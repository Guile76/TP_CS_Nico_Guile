﻿using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows;

namespace Data_Access_Layer
{
    public static class AssistantSqlMetier
    {
        // Attribut
        private static string nomTable = AssistantSql.Table_Metiers;

        /// <summary>
        /// Ajoute une entrée à Table_Metiers
        /// </summary>
        /// <param name="intitule">Nom de la nouvelle entrée (PK)</param>
        /// <param name="ageMinimum">Age minimum requis pour la nouvelle entrée</param>
        /// <returns>True si l'entrée à bien été créée, false dans le cas contraire</returns>
        public static bool Ajouter(EMetier intitule, int ageMinimum)
        {
            bool resultat = false;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"INSERT INTO {0} (intitule, age_minimum) values (@intitule, @age_minimum)", nomTable);

                        commandeBDD.Parameters.AddWithValue("@intitule", intitule);
                        commandeBDD.Parameters.AddWithValue("@age_minimum", ageMinimum);

                        commandeBDD.ExecuteNonQuery();

                        resultat = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Supprime une entrée de Table_Metiers
        /// </summary>
        /// <param name="intitule"></param>
        /// <returns></returns>
        public static bool Supprimer(EMetier intitule)
        {
            bool resultat = false;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"DELETE FROM {0} WHERE intitule = @intitule", nomTable);

                        commandeBDD.Parameters.AddWithValue("@intitule", intitule);

                        commandeBDD.ExecuteNonQuery();

                        resultat = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Vérifie si une entrée existe dans Table_Metiers
        /// </summary>
        /// <param name="intitule">Entrée à rechercher</param>
        /// <returns>True si l'entrée existe, false dans le cas contraire</returns>
        public static bool Existe(EMetier intitule)
        {
            bool resultat = false;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"SELECT * FROM {0} WHERE intitule = @intitule", nomTable);

                        commandeBDD.Parameters.AddWithValue("@intitule", intitule);

                        string compte = Convert.ToString(commandeBDD.ExecuteScalar());

                        if (compte != null)
                        {
                            resultat = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Modifie une entrée de Table_Metiers
        /// </summary>
        /// <param name="intitule">Nom de l'entrée à modifier</param>
        /// <param name="ageMinimum">Age de l'entrée à modifier</param>
        /// <returns>True si l'entrée à bien été modifiée, false dans le cas contraire</returns>
        public static bool Modifier(EMetier intitule, int ageMinimum)
        {
            bool resultat = false;
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);

            SqlConnection connexion = new SqlConnection(connecterABDD);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        if (Existe(intitule))
                        {
                            commandeBDD.CommandText = string.Format(@"UPDATE {0} SET intitule = @intitule, age_minimum = @age_minimum where intitule = @intitule", nomTable);

                            commandeBDD.Parameters.AddWithValue("@intitule", (int)intitule);
                            commandeBDD.Parameters.AddWithValue("@age_minimum", ageMinimum);

                            commandeBDD.ExecuteNonQuery();

                            resultat = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Extrait un tableau de Table_Metiers selon multi-critères
        /// </summary>
        /// <param name="intitule">Nom recherché (nullable)</param>
        /// <param name="ageMinimum">Age recherché (nullable)</param>
        /// <returns></returns>
        public static List<Metier> ObtenirTableauRecherche(EMetier? intitule, int? ageMinimum)
        {
            // Variables
            List<Metier> liste = new List<Metier>();
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);
            SqlConnection connexion = new SqlConnection(connecterABDD);

            // Connexion
            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        string texteCommande = string.Format(@"SELECT * FROM {0} ", nomTable);

                        if(intitule != null)
                        {
                            texteCommande += "WHERE intitule = @intitule";
                        } else
                        {
                            texteCommande += "WHERE intitule IS NOT NULL";
                        }

                        if (ageMinimum != null)
                        {
                            texteCommande += " AND age_minimum <= @age_minimum";
                        }

                        commandeBDD.CommandText = texteCommande;

                        if(intitule != null)
                        {
                            commandeBDD.Parameters.AddWithValue("@intitule", intitule);
                        }

                        if (ageMinimum != null)
                        {
                            commandeBDD.Parameters.AddWithValue("@age_minimum", ageMinimum);
                        }

                        DataTable table = new DataTable();
                        DataSet dataSet = new DataSet();
                        SqlDataAdapter adaptateur = new SqlDataAdapter(commandeBDD);
                        adaptateur.Fill(dataSet);

                        table = dataSet.Tables[0];

                        DataRowCollection rangs = table.Rows;

                        foreach (DataRow data in rangs)
                        {
                            Metier metier = new Metier();
                            metier.Intitule = (EMetier)Convert.ToInt32(data["intitule"]);
                            metier.AgeMinimum = Convert.ToInt32(data["age_minimum"]);

                            liste.Add(metier);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return liste;
        }

        /// <summary>
        /// Extrait le tableau complet de Table_Metiers
        /// </summary>
        /// <returns>Une liste d'objets de type Metier</returns>
        public static List<Metier> ObtenirTableauComplet()
        {
            // Variables
            List<Metier> liste = new List<Metier>();
            string connecterABDD = string.Format(@"Server={0};Integrated Security=SSPI;Initial Catalog={1};Connect Timeout=30", AssistantSql.Serveur, AssistantSql.Nom_Bdd);
            SqlConnection connexion = new SqlConnection(connecterABDD);

            // Connexion
            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = string.Format(@"SELECT * FROM {0}", nomTable);

                        DataTable table = new DataTable();
                        DataSet dataSet = new DataSet();
                        SqlDataAdapter adaptateur = new SqlDataAdapter(commandeBDD);

                        adaptateur.Fill(dataSet);

                        table = dataSet.Tables[0];

                        DataRowCollection rangs = table.Rows;

                        foreach (DataRow data in rangs)
                        {
                            Metier metier = new Metier();
                            metier.Intitule = (EMetier)Convert.ToInt32(data["intitule"]);
                            metier.AgeMinimum = Convert.ToInt32(data["age_minimum"]);

                            liste.Add(metier);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return liste;
        }

        
    }
}
