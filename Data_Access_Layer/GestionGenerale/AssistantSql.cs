﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Data_Access_Layer
{
    public static partial class AssistantSql
    {
        // Attributs
        private static string serveur = @"localhost\SQLEXPRESS";
        private static string nom_bdd = "Populate_DB";

        // Propriété
        public static string Nom_Bdd { get { return nom_bdd; } }
        public static string Serveur { get { return serveur; } set { serveur = value; } }

        // Méthodes

        /// <summary>
        /// Teste la connexion au serveur
        /// </summary>
        /// <param name="serveur">Nom du serveur</param>
        /// <returns>True si la connexion réussi, false dans le cas contraire</returns>
        public static bool TesterServeur(string serveur)
        {
            string connexionString = string.Format(@"Server={0};Integrated Security=SSPI;database=master;Connect Timeout=1", serveur);
            bool resultat;

            SqlConnection connexion = new SqlConnection(connexionString);

            try
            {
                using (connexion)
                {
                    connexion.Open();
                    resultat = true;
                }
            }
            catch (System.Exception ex)
            {
                Serveur = @"localhost";
                resultat = false;
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }
            return resultat;
        }

        /// <summary>
        /// Passe une commande SQL à la base de données master
        /// </summary>
        /// <param name="commande">Commande SQL en string</param>
        private static bool PasserCommandeMaster(string commande)
        {
            bool resultat = false;
            string connecterAMaster = string.Format(@"Server={0};Integrated Security=SSPI;database=master", serveur);

            SqlConnection connexion = new SqlConnection(connecterAMaster);

            try
            {
                using (connexion)
                {
                    connexion.Open();

                    using (SqlCommand commandeBDD = connexion.CreateCommand())
                    {
                        commandeBDD.CommandText = commande;

                        commandeBDD.ExecuteNonQuery();

                        resultat = true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (connexion.State == System.Data.ConnectionState.Open)
                {
                    connexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Vérifie l'existence de la base de données
        /// </summary>
        /// <returns>True si la BDD existe, False dans le cas contraire</returns>
        public static bool VerifierExistenceBDD()
        {
            string str = string.Format("SELECT DB_ID('{0}')", nom_bdd);
            string connecterAMaster = string.Format(@"Server={0};Integrated Security=SSPI;database=master", serveur);
            bool resultat = false;

            SqlConnection maConnexion = new SqlConnection(connecterAMaster);

            try
            {
                using (maConnexion)
                {
                    maConnexion.Open();

                    using (SqlCommand maCommande = maConnexion.CreateCommand())
                    {
                        maCommande.CommandText = str;
                        resultat = (maCommande.ExecuteScalar() != DBNull.Value);
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Programme", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            finally
            {
                if (maConnexion.State == System.Data.ConnectionState.Open)
                {
                    maConnexion.Close();
                }
            }

            return resultat;
        }

        /// <summary>
        /// Crée la base de données de l'application
        /// </summary>
        public static bool CreerBDD()
        {
            string commande = string.Format("CREATE DATABASE {0}", nom_bdd);

            return PasserCommandeMaster(commande);
        }

        /// <summary>
        /// Supprime la base de données de l'application
        /// </summary>
        public static bool SupprimerBDD()
        {
            string commande = string.Format("ALTER DATABASE {0} SET SINGLE_USER WITH ROLLBACK IMMEDIATE " +
                "USE master DROP DATABASE {0}", nom_bdd);

            return PasserCommandeMaster(commande);
        }
    }
}
