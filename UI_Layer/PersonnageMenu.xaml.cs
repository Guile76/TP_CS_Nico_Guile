﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UI_Layer
{
    /// <summary>
    /// Logique d'interaction pour PersonnageMenu.xaml
    /// </summary>
    public partial class PersonnageMenu : Window
    {
        public PersonnageMenu()
        {
            InitializeComponent();
        }

        private void btn_backClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_createClick(object sender, RoutedEventArgs e)
        {
            FenetreAjoutVillageois uneFenetrePersonnage = new FenetreAjoutVillageois();
            this.Hide();
            uneFenetrePersonnage.ShowDialog();
            this.ShowDialog();
        }

        private void btn_character_deal_Click(object sender, RoutedEventArgs e)
        {
            FenetreAffichagePersonnages fenetreAffichagePersonnages = new FenetreAffichagePersonnages();
            this.Hide();
            fenetreAffichagePersonnages.ShowDialog();
            this.ShowDialog();
        }
    }
}
