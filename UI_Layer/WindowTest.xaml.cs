﻿using Business_Layer;
using Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace UI_Layer
{
    /// <summary>
    /// Interaction logic for WindowText.xaml
    /// </summary>
    public partial class WindowTest : Window
    {
        public WindowTest()
        {
            InitializeComponent();

            AfficherExistenceBDD();
            cb_liste_races.ItemsSource = Enum.GetValues(typeof(ERace));
            cb_liste_sexes.ItemsSource = Enum.GetValues(typeof(ESexe));
        }

        private void AfficherExistenceBDD()
        {
            if (GestionnaireBDD.EstBDDExiste())
            {
                tb_test_info_existence_bdd.Background = Brushes.ForestGreen;
                if (btn_test_creer_bdd.IsEnabled)
                {
                    btn_test_creer_bdd.IsEnabled = false;
                }
                if (!btn_test_supprimer_bdd.IsEnabled)
                {
                    btn_test_supprimer_bdd.IsEnabled = true;
                }
            }
            else
            {
                tb_test_info_existence_bdd.Background = Brushes.Red;
                if (!btn_test_creer_bdd.IsEnabled)
                {
                    btn_test_creer_bdd.IsEnabled = true;
                }
                if (btn_test_supprimer_bdd.IsEnabled)
                {
                    btn_test_supprimer_bdd.IsEnabled = false;
                }
            }
        }

        private void btn_creer_bdd_Click(object sender, RoutedEventArgs e)
        {
            GestionnaireBDD.CreationBDD();
            GestionnaireBDD.CreationTables();
            AfficherExistenceBDD();
        }

        private void btn_supprimer_bdd_Click(object sender, RoutedEventArgs e)
        {
            GestionnaireBDD.SuppressionBDD();
            AfficherExistenceBDD();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult resultat = MessageBox.Show("Voulez-vous quitter ?", "Quitter le programme", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if(resultat == MessageBoxResult.Yes)
            {
                Close();
            }
        }

        private void btn_test_creer_personnage_Click(object sender, RoutedEventArgs e)
        {
            long indice;
            ERace race = GestionnaireRace.GenererAleatoirement();
            ESexe sexe = GestionnaireSexe.GenererAleatoirement();
            Nom nom = GestionnaireNom.GenererAleatoirement(race);
            Prenom prenom = GestionnairePrenom.GenererAleatoirement(race, sexe);
            int age = new Random().Next(50);
            Metier metier = GestionnaireMetier.GenererAleatoirement(age);
            
            /* Ajoute une entrée personnage. Le "out indice" met jour un indice de type long pour savoir l'indice
             * nouvellement créé. Tu n'es pas obligé d'appeler la méthode dans un if, elle se lance aussi seule */
            if (GestionnairePersonnage.Ajouter(out indice, nom.Proposition, prenom.Proposition, age, race, sexe, metier.Intitule))
            {
                MessageBox.Show(string.Format("Personnage créé avec l'indice {0}", indice.ToString()));
            }
        }

        private void btn_test_supprimer_personnage_Click(object sender, RoutedEventArgs e)
        {
            long indice;
            if(long.TryParse(tb_indice_a_supprimer.Text, out indice))
            {
                /* Pareil que pour la création d'un personnage. Tu dois mettre un indice de type Long et tu n'es
                 * pas obligé de le lancer dans un if. */
                if (GestionnairePersonnage.Supprimer(indice))
                {
                    tb_indice_a_supprimer.Text = "";
                    MessageBox.Show("Entrée supprimée");
                }
                else
                {
                    tb_indice_a_supprimer.Text = "";
                    MessageBox.Show("Entrée inexistante");
                }
            }
        }

        private void btn_test_modifier_personnage_Click(object sender, RoutedEventArgs e)
        {
            long indice;
            ERace race = GestionnaireRace.GenererAleatoirement();
            ESexe sexe = GestionnaireSexe.GenererAleatoirement();
            Nom nom = GestionnaireNom.GenererAleatoirement(race);
            Prenom prenom = GestionnairePrenom.GenererAleatoirement(race, sexe);
            int age = new Random().Next(50);
            Metier metier = GestionnaireMetier.GenererAleatoirement(age);

            if (long.TryParse(tb_indice_a_supprimer.Text, out indice))
            {
                /* Modification du personnage, pareil que la création/suppression. Tu dois mentionner l'indice
                 * de l'entrée à modifier. Tu n'es pas obligé de le lancer dans un if. */
                if (GestionnairePersonnage.Modifier(indice, nom.Proposition, prenom.Proposition, age, race, sexe, metier.Intitule))
                {
                    tb_indice_a_supprimer.Text = "";
                    MessageBox.Show("Modification effectuée");
                }
                else
                {
                    tb_indice_a_supprimer.Text = "";
                    MessageBox.Show("Modification non effectuée");
                }
            }
        }

        private void btn_tableau_metier_alea_Click(object sender, RoutedEventArgs e)
        {
            /* Recherche multi-critères qui renvoie une List<Metier>. */

            int age = new Random().Next(50);

            List<Metier> liste = GestionnaireMetier.ObtenirTableauParRecherche(null, age);
            
            string message = string.Format("Age : {0}\n", age);

            if(liste.Count > 0)
            {
                foreach (Metier m in liste)
                {
                    message += string.Format("Métier : ({0}) - Age : {1}\n", m.Intitule, m.AgeMinimum);
                }

                MessageBox.Show(message, "Liste de métiers");
            }
            else
            {
                MessageBox.Show("Aucune entrée", "Liste des métiers");
            }
            
        }

        private void btn_tableau_tous_Click(object sender, RoutedEventArgs e)
        {
            /* Pareil que la recherche multi-critères pour le renvoi. */
            List<Personnage> liste = GestionnairePersonnage.ObtenirTableauComplet();
            string message = "";

            if (liste.Count > 0)
            {
                foreach (Personnage p in liste)
                {
                    message += string.Format("({0}) {1} {2}, age : {3}, sexe : {4}, race : {5}, métier : {6}\n", p.Id, p.Prenom, p.Nom, p.Age, p.Sexe, p.Race, p.Metier);
                }

                MessageBox.Show(message, "Liste des personnages existants");
            }
            else
            {
                MessageBox.Show("Aucune entrée", "Liste des personnages existants");
            }
        }

        private void btn_tableau_tous_metiers_Click(object sender, RoutedEventArgs e)
        {
            List<Metier> liste = GestionnaireMetier.ObtenirTableauComplet();
            string message = "";

            if (liste.Count > 0)
            {
                foreach (Metier m in liste)
                {
                    message += string.Format("Métier : {0} - Age minimum : {1}\n", m.Intitule, m.AgeMinimum);
                }

                MessageBox.Show(message, "Liste des métiers existants");
            }
            else
            {
                MessageBox.Show("Aucune entrée", "Liste des personnages existants");
            }
        }

        private void btn_tableau_nom_aleatoire_Click(object sender, RoutedEventArgs e)
        {
            if(cb_liste_races.SelectedItem != null && cb_liste_sexes.SelectedItem != null)
            {
                Prenom prenom = GestionnairePrenom.GenererAleatoirement((ERace)cb_liste_races.SelectedIndex, (ESexe)cb_liste_sexes.SelectedIndex);
                Nom nom = GestionnaireNom.GenererAleatoirement((ERace)cb_liste_races.SelectedIndex);

                MessageBox.Show(string.Format("{0} {1} - Race : {2} - Sexe : {3}", prenom.Proposition, nom.Proposition, cb_liste_races.Text, cb_liste_sexes.Text), "Génération aléatoire", MessageBoxButton.OK, MessageBoxImage.Information);

                cb_liste_races.SelectedItem = cb_liste_sexes.SelectedItem = null;
            }
            else
            {
                MessageBox.Show("Veuillez sélectionner une valeur pour les deux listes à gauche.", "Notification", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            

            
        }

        private void btn_tableau_metier_aleatoire_Click(object sender, RoutedEventArgs e)
        {
            int age;
            if(int.TryParse(tb_age_metier.Text, out age))
            {
                Metier metier = GestionnaireMetier.GenererAleatoirement(age);

                MessageBox.Show(string.Format("Métier : {0} - Age minimum : {1}", metier.Intitule, metier.AgeMinimum), "Génération aléatoire", MessageBoxButton.OK, MessageBoxImage.Information);

                tb_age_metier.Text = null;
            }
            else
            {
                MessageBox.Show("Veuillez entrer un age valide (entier).", "Notification", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}