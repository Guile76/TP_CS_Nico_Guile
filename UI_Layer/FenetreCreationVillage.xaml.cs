﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UI_Layer
{
    /// <summary>
    /// Logique d'interaction pour FenetreCreationVillage.xaml
    /// </summary>
    public partial class FenetreCreationVillage : Window
    {
        private List<String> liste_village;
        public List<String> Liste_village
        {
            get
            {
                return new List<string>() { "toto", "tata", "titi", "Gros minet" };
            }
            set { liste_village = value; }
        }

        public FenetreCreationVillage()
        {
            InitializeComponent();
        }

        private void txt_nomVillageChanged(object sender, TextChangedEventArgs e)
        {
            if (txt_nomVillage.Text != "")
            {
                txt_nomVillage.IsReadOnly = false;
                btn_creationVide.SetValue(TextBlock.FontWeightProperty, FontWeights.Bold);

                btn_allRandom.SetValue(TextBlock.FontWeightProperty, FontWeights.Normal);
                txt_nomVillage2.IsReadOnly = true;
                btn_allRandom.IsEnabled = false;
                btn_creationVide.IsEnabled = true;
                btn_creationPlein.IsEnabled = false;
            }
            else
            {
                btn_creationVide.SetValue(TextBlock.FontWeightProperty, FontWeights.Normal);
                btn_allRandom.SetValue(TextBlock.FontWeightProperty, FontWeights.Bold);
                txt_nomVillage2.IsReadOnly = false;
                btn_allRandom.IsEnabled = true;
                btn_creationVide.IsEnabled = false;
            }
        }

        private void txt_nomVillage2Changed(object sender, TextChangedEventArgs e)
        {
            if (txt_nomVillage2.Text != "")
            {
                txt_nomVillage2.IsReadOnly = false;
                btn_creationPlein.SetValue(TextBlock.FontWeightProperty, FontWeights.Bold);

                btn_allRandom.SetValue(TextBlock.FontWeightProperty, FontWeights.Normal);
                txt_nomVillage.IsReadOnly = true;
                btn_allRandom.IsEnabled = false;
                btn_creationVide.IsEnabled = false;
                btn_creationPlein.IsEnabled = true;
            }
            else
            {
                btn_creationPlein.SetValue(TextBlock.FontWeightProperty, FontWeights.Normal);
                btn_allRandom.SetValue(TextBlock.FontWeightProperty, FontWeights.Bold);
                txt_nomVillage.IsReadOnly = false;
                btn_allRandom.IsEnabled = true;
                btn_creationPlein.IsEnabled = false;
            }

        }

        private void btn_retourClick(object sender, RoutedEventArgs e)
        {
            MainWindow myMain = new MainWindow();
            myMain.Show();
            this.Close();
        }
    }
}
