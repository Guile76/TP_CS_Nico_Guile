﻿#pragma checksum "..\..\FenetreAjoutVillageois.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "2142EEFED2DD4DE53047D09C797B871F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using UI_Layer;


namespace UI_Layer {
    
    
    /// <summary>
    /// FenetreAjoutVillageois
    /// </summary>
    public partial class FenetreAjoutVillageois : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_nom;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label1;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_prenom;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label2;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label3;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_age;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label4;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbx_race;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label5;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbx_metier;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label6;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbx_village;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_ajouter;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_retour;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbx_sexe;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_allRan;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\FenetreAjoutVillageois.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_create_character_initialize;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/UI_Layer;component/fenetreajoutvillageois.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\FenetreAjoutVillageois.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.txt_nom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.label1 = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.txt_prenom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.label2 = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.label3 = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.txt_age = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.label4 = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.cbx_race = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 10:
            this.label5 = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.cbx_metier = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 12:
            this.label6 = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.cbx_village = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 14:
            this.btn_ajouter = ((System.Windows.Controls.Button)(target));
            
            #line 30 "..\..\FenetreAjoutVillageois.xaml"
            this.btn_ajouter.Click += new System.Windows.RoutedEventHandler(this.btn_ajouterClick);
            
            #line default
            #line hidden
            return;
            case 15:
            this.btn_retour = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\FenetreAjoutVillageois.xaml"
            this.btn_retour.Click += new System.Windows.RoutedEventHandler(this.btn_retourClick);
            
            #line default
            #line hidden
            return;
            case 16:
            this.cbx_sexe = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 17:
            this.btn_allRan = ((System.Windows.Controls.Button)(target));
            
            #line 37 "..\..\FenetreAjoutVillageois.xaml"
            this.btn_allRan.Click += new System.Windows.RoutedEventHandler(this.btn_allRanClick);
            
            #line default
            #line hidden
            return;
            case 18:
            this.btn_create_character_initialize = ((System.Windows.Controls.Button)(target));
            
            #line 38 "..\..\FenetreAjoutVillageois.xaml"
            this.btn_create_character_initialize.Click += new System.Windows.RoutedEventHandler(this.btn_create_character_initialize_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

