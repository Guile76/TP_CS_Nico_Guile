﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UI_Layer
{
    /// <summary>
    /// Logique d'interaction pour communauteMenu.xaml
    /// </summary>
    public partial class communauteMenu : Window
    {
        public communauteMenu()
        {
            InitializeComponent();
        }

        private void btn_backClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_createClick(object sender, RoutedEventArgs e)
        {
            WorkInProgress fenWork = new WorkInProgress();
            this.Hide();
            fenWork.ShowDialog();
            this.ShowDialog();
        }

        private void btn_consulteClick(object sender, RoutedEventArgs e)
        {
            WorkInProgress fenWork = new WorkInProgress();
            this.Hide();
            fenWork.ShowDialog();
            this.ShowDialog();
        }

        private void btn_eraseClick(object sender, RoutedEventArgs e)
        {
            WorkInProgress fenWork = new WorkInProgress();
            this.Hide();
            fenWork.ShowDialog();
            this.ShowDialog();
        }
    }
}
