﻿using Business_Layer;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UI_Layer;

namespace UI_Layer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            GestionnaireBDD.EstBDDExiste();
        }


        private void btn_testClick(object sender, RoutedEventArgs e)
        {
            WindowTest Win = new UI_Layer.WindowTest();
            this.Hide();
            Win.ShowDialog();
            this.ShowDialog();

        }

        private void btn_communauteClick(object sender, RoutedEventArgs e)
        {
            communauteMenu fenetreCommunaute = new communauteMenu();
            this.Hide();
            fenetreCommunaute.ShowDialog();
            this.ShowDialog();
        }

        private void btn_quitterclick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_ajoutPersoClick(object sender, RoutedEventArgs e)
        {
            PersonnageMenu fenetrePersonnage = new PersonnageMenu();
            this.Hide();
            fenetrePersonnage.ShowDialog();
            this.ShowDialog();
        }
    }
}
