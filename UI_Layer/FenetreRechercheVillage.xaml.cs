﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UI_Layer
{
    /// <summary>
    /// Logique d'interaction pour FenetreRechercheVillage.xaml
    /// </summary>
    public partial class FenetreRechercheVillage : Window
    {
        public FenetreRechercheVillage()
        {
            InitializeComponent();
        }

        private void btn_rechercheClick(object sender, RoutedEventArgs e)
        {
            string uneRecherche;
            uneRecherche = txt_rechercheVillage.Text;
            // MessageBox.Show(uneRecherche);
        }

        private void btn_retourClick(object sender, RoutedEventArgs e)
        {
            MainWindow myMain = new MainWindow();
            myMain.Show();
            this.Close();
        }
    }
}
