﻿using Business_Layer;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UI_Layer
{
    /// <summary>
    /// Interaction logic for FenetreAffichagePersonnages.xaml
    /// </summary>
    public partial class FenetreAffichagePersonnages : Window
    {
        private List<Personnage> liste;
        public List<Personnage> Liste { get { return liste; } set { liste = value; } }
        public FenetreAffichagePersonnages()
        {
            InitializeComponent();
            cbx_affichage_personnages_sexe.ItemsSource = Enum.GetValues(typeof(ESexe));
            cbx_affichage_personnages_race.ItemsSource = Enum.GetValues(typeof(ERace));
            cbx_affichage_personnages_metier.ItemsSource = Enum.GetValues(typeof(EMetier));
            cbx_affichage_personnages_sexe_modif.ItemsSource = Enum.GetValues(typeof(ESexe));
            cbx_affichage_personnages_race_modif.ItemsSource = Enum.GetValues(typeof(ERace));
            cbx_affichage_personnages_metier_modif.ItemsSource = Enum.GetValues(typeof(EMetier));

            AFficherTout();
        }

        private void btn_affichage_personnages_trouver_Click(object sender, RoutedEventArgs e)
        {
            string nom, prenom;
            int? age;
            int tampon;
            ERace? race;
            ESexe? sexe;
            EMetier? metier;

            if (tb_affichage_personnage_nom.Text == "")
            {
                nom = null;
            }
            else
            {
                nom = tb_affichage_personnage_nom.Text;
            }

            if (tb_affichage_personnage_prenom.Text == "")
            {
                prenom = null;
            }
            else
            {
                prenom = tb_affichage_personnage_prenom.Text;
            }

            if (int.TryParse(tb_affichage_personnage_age.Text, out tampon))
            {
                age = tampon;
            }
            else
            {
                age = null;
            }

            if (cbx_affichage_personnages_race.SelectedItem == null)
            {
                race = null;
            }
            else
            {
                race = (ERace)cbx_affichage_personnages_race.SelectedItem;
            }

            if (cbx_affichage_personnages_sexe.SelectedItem == null)
            {
                sexe = null;
            }
            else
            {
                sexe = (ESexe)cbx_affichage_personnages_sexe.SelectedItem;
            }

            if (cbx_affichage_personnages_metier.SelectedItem == null)
            {
                metier = null;
            }
            else
            {
                metier = (EMetier)cbx_affichage_personnages_metier.SelectedItem;
            }

            List<Personnage> liste = GestionnairePersonnage.ObtenirTableauParRecherche(nom, prenom, age, race, sexe, metier);
            dg_affichage_personnages.ItemsSource = liste;

            dg_affichage_personnages.SelectedItem = 1;

            InitialiserChampsModif();
            BloquerModif();
        }

        private void tb_affichage_personnage_age_LostFocus(object sender, RoutedEventArgs e)
        {
            int test;
            if (!int.TryParse(((TextBox)sender).Text, out test) && ((TextBox)sender).Text != "")
            {
                MessageBox.Show("Utilisez un entier pour l'âge.", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                ((TextBox)sender).Text = "";
            }
        }

        private void btn_affichage_personnages_initialiser_Click(object sender, RoutedEventArgs e)
        {
            AFficherTout();
        }

        private void AFficherTout()
        {
            liste = GestionnairePersonnage.ObtenirTableauParRecherche(null, null, null, null, null, null);
            dg_affichage_personnages.ItemsSource = liste;
            InitialiserChampsRecherche();
            InitialiserChampsModif();
            BloquerModif();

        }

        private void dg_affichage_personnages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dg_affichage_personnages.SelectedIndex != -1)
            {
                Personnage personnage = (Personnage)dg_affichage_personnages.SelectedItem;
                tb_affichage_personnage_id_modif.Text = personnage.Id.ToString();
                tb_affichage_personnage_nom_modif.Text = personnage.Nom;
                tb_affichage_personnage_prenom_modif.Text = personnage.Prenom;
                tb_affichage_personnage_age_modif.Text = personnage.Age.ToString();
                cbx_affichage_personnages_sexe_modif.SelectedIndex = (int)personnage.Sexe;
                cbx_affichage_personnages_race_modif.SelectedIndex = (int)personnage.Race;
                cbx_affichage_personnages_metier_modif.SelectedIndex = (int)personnage.Metier;

                DebloquerModif();
            }
        }

        private void InitialiserChampsRecherche()
        {
            tb_affichage_personnage_nom.Text = "";
            tb_affichage_personnage_prenom.Text = "";
            tb_affichage_personnage_age.Text = "";
            cbx_affichage_personnages_sexe.SelectedItem = null;
            cbx_affichage_personnages_race.SelectedItem = null;
            cbx_affichage_personnages_metier.SelectedItem = null;
        }

        private void InitialiserChampsModif()
        {
            tb_affichage_personnage_id_modif.Text = "";
            tb_affichage_personnage_nom_modif.Text = "";
            tb_affichage_personnage_prenom_modif.Text = "";
            tb_affichage_personnage_age_modif.Text = "";
            cbx_affichage_personnages_sexe_modif.SelectedItem = null;
            cbx_affichage_personnages_race_modif.SelectedItem = null;
            cbx_affichage_personnages_metier_modif.SelectedItem = null;
        }

        private void BloquerModif()
        {
            if (tb_affichage_personnage_nom_modif.IsEnabled)
            {
                tb_affichage_personnage_nom_modif.IsEnabled = false;
                tb_affichage_personnage_prenom_modif.IsEnabled = false;
                tb_affichage_personnage_age_modif.IsEnabled = false;
                cbx_affichage_personnages_sexe_modif.IsEnabled = false;
                cbx_affichage_personnages_race_modif.IsEnabled = false;
                cbx_affichage_personnages_metier_modif.IsEnabled = false;
            }
        }

        private void DebloquerModif()
        {
            if (!tb_affichage_personnage_nom_modif.IsEnabled)
            {
                tb_affichage_personnage_nom_modif.IsEnabled = true;
                tb_affichage_personnage_prenom_modif.IsEnabled = true;
                tb_affichage_personnage_age_modif.IsEnabled = true;
                cbx_affichage_personnages_sexe_modif.IsEnabled = true;
                cbx_affichage_personnages_race_modif.IsEnabled = true;
                cbx_affichage_personnages_metier_modif.IsEnabled = true;
            }
        }

        private void btn_affichage_personnage_modifier_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult resultat = MessageBox.Show("Voulez-vous vraiment effectuer cette modification ?", "Personnage", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (resultat == MessageBoxResult.Yes)
            {
                long indice;
                if (long.TryParse(tb_affichage_personnage_id_modif.Text, out indice))
                {
                    if (GestionnairePersonnage.Modifier(indice, tb_affichage_personnage_nom_modif.Text, tb_affichage_personnage_prenom_modif.Text, int.Parse(tb_affichage_personnage_age_modif.Text), (ERace)cbx_affichage_personnages_race_modif.SelectedIndex, (ESexe)cbx_affichage_personnages_sexe_modif.SelectedIndex, (EMetier)cbx_affichage_personnages_metier_modif.SelectedIndex))
                    {
                        MessageBox.Show("Modification effectuée", "Personnage", MessageBoxButton.OK, MessageBoxImage.Information);

                        AFficherTout();
                    }
                    else
                    {
                        MessageBox.Show("Echec de la modification", "Personnage", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btn_affichage_personnage_supprimer_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult resultat = MessageBox.Show("Voulez-vous vraiment effectuer cette suppression ?", "Personnage", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (resultat == MessageBoxResult.Yes)
            {
                long indice;
                if (long.TryParse(tb_affichage_personnage_id_modif.Text, out indice))
                {
                    if (GestionnairePersonnage.Supprimer(indice))
                    {
                        MessageBox.Show("Modification effectuée", "Personnage", MessageBoxButton.OK, MessageBoxImage.Information);
                        AFficherTout();
                    }
                    else
                    {
                        MessageBox.Show("Modification échouée", "Personnage", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btn_affichage_personnage_retour_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
