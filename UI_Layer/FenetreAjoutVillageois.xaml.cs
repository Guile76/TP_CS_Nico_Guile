﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;
using Business_Layer;

namespace UI_Layer
{
    /// <summary>
    /// Logique d'interaction pour FenetreAjoutVillageois.xaml
    /// </summary>
    public partial class FenetreAjoutVillageois : Window
    {
        public FenetreAjoutVillageois()
        {
            InitializeComponent();

            cbx_race.ItemsSource = Enum.GetValues(typeof(ERace));
            cbx_sexe.ItemsSource = Enum.GetValues(typeof(ESexe));
            cbx_metier.ItemsSource = Enum.GetValues(typeof(EMetier));

            cbx_sexe.SelectedItem = GestionnaireSexe.GenererAleatoirement();
            //test
        }

        private void btn_retourClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void btn_allRanClick(object sender, RoutedEventArgs e)
        {
            txt_nom.IsReadOnly = true;
            txt_age.IsReadOnly = true;
            txt_prenom.IsReadOnly = true;
            cbx_metier.IsHitTestVisible = false;
            cbx_race.IsHitTestVisible = false;
            cbx_sexe.IsHitTestVisible = false;

            ERace race = GestionnaireRace.GenererAleatoirement();
            ESexe sexe = GestionnaireSexe.GenererAleatoirement();
            Nom nom = GestionnaireNom.GenererAleatoirement(race);
            Prenom prenom = GestionnairePrenom.GenererAleatoirement(race, sexe);
            int age = new Random().Next(50);
            Metier metier = GestionnaireMetier.GenererAleatoirement(age);

            cbx_sexe.SelectedItem = sexe;
            cbx_race.SelectedItem = race;
            txt_nom.Text = nom.ToString();
            txt_prenom.Text = prenom.ToString();
            cbx_metier.SelectedItem = metier.Intitule;
            txt_age.Text = "" + age;
        }

        private void btn_ajouterClick(object sender, RoutedEventArgs e)
        {
            long indice;
            ERace race = (ERace)cbx_race.SelectedItem;
            ESexe sexe = (ESexe)cbx_sexe.SelectedItem;
            Nom nom = new Nom(txt_nom.Text, race);
            Prenom prenom = new Prenom(txt_prenom.Text, race, sexe);
            int age = new Random().Next(50);
            EMetier metier = (EMetier)cbx_metier.SelectedItem;

            if (GestionnairePersonnage.Ajouter(out indice, nom.Proposition, prenom.Proposition, age, race, sexe, metier))
            {
                MessageBox.Show(string.Format("Personnage créé avec l'indice {0}", indice.ToString()));
            }
        }

        private void btn_create_character_initialize_Click(object sender, RoutedEventArgs e)
        {
            cbx_sexe.SelectedItem = null;
            cbx_race.SelectedItem = null;
            txt_nom.Text = "";
            txt_prenom.Text = "";
            cbx_metier.SelectedItem = null;
            txt_age.Text = "";

            if (txt_nom.IsReadOnly)
            {
                txt_nom.IsReadOnly = false;
                txt_age.IsReadOnly = false;
                txt_prenom.IsReadOnly = false;
            }

            if (!cbx_metier.IsHitTestVisible)
            {
                cbx_metier.IsHitTestVisible = true;
                cbx_race.IsHitTestVisible = true;
                cbx_sexe.IsHitTestVisible = true;
            }
            
        }
    }
}
