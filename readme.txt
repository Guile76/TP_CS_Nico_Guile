POPULATE

Travail pratique pour le cours de C#
Nicolas et Guillaume

Projet : application de gestion des personnages non joueurs dans une communauté
 d'univers heroic-fantasy (utilitaire pour les parties de jeux de rôles).

Outils utilisés : Visual Studio, .NET (WPF et ADO.NET), SQL Server

Fonctionnalités :
- Personnages : création, suppression, modification, recherches multi-critères.
- Tables de données de noms, prénoms et métiers pour génération aléatoire.
- Auto-création de la base de données si cette dernières n'est pas installée (en cas de besoin, changer le serveur dans
Data_Access_Layer/GestionGenerale/AssistantSql.cs).
- Génération automatique de données et de personnages.

Fonctionnalités prévues :
- Gestion de communautés (villages, villes...) : ajout, suppression, modification, recherches multi-critères.
- Gestion des liens entre personnages et communautés.
- Génération aléatoires de communautés et des personnages qui la composent.
- Export des listes au format PDF.

Bugs connus :
- Lors de la création d'un personnage, si l'âge entré est inférieur à l'âge requis pour avoir le métier choisi,
l'application attribue un âge aléatoire respectant les critères du métier (possibilité de changer l'âge
dans la gestion des personnages).