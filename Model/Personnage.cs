﻿

namespace Model
{
    public class Personnage
    {
        // Propriétés
        public long Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public int Age { get; set; }
        public ERace Race { get; set; }
        public ESexe Sexe { get; set; }
        public EMetier Metier { get; set; }

        // Constructeurs
        public Personnage()
        {
            Id = 0;
            Nom = "defaut";
            Prenom = "defaut";
            Age = 1;
            Race = ERace.DEMI_ELFE;
            Sexe = ESexe.MALE;
            Metier = EMetier.AUBERGISTE;
        }

        public Personnage(string nom, string prenom, int age, ERace race, ESexe sexe, EMetier metier)
        {
            Id = 0;
            Nom = nom;
            Prenom = prenom;
            Age = age;
            Race = race;
            Sexe = sexe;
            Metier = metier;
        }
    }
}
