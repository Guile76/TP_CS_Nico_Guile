﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Nom
    {
        // Attributs
        public long Id { get; set; }
        public string Proposition { get; set; }
        public ERace Race { get; set; }


        public override string ToString()
        {
            return Proposition;
        }
        // Constructeur
        public Nom()
        {

        }

        public Nom(string proposition, ERace race)
        {
            Id = 0;
            Proposition = proposition;
            Race = race;
        }

    }
}
