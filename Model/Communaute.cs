﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Communaute
    {
        // Attributs
        public long Id { get; set; }
        public string Intitule { get; set; }
        public int Taille { get; set; }

        // Constructeur
        public Communaute(string intitule, int taille)
        {
            Id = 0;
            Intitule = intitule;
            Taille = taille;
        }
    }
}
