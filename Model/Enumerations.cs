﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public enum ERace
    {
        HUMAIN,
        NAIN,
        ELFE,
        GNOME,
        DEMI_ORC, // N'utilise que des prénoms
        DEMI_ELFE, // Utilise noms et prénoms elfes ou humains
        DRAKEIDE,
        HALFELIN,
        TIEFELIN
    }

    public enum ESexe
    {
        [Description("Mâle")]MALE,
        [Description("Femelle")]FEMELLE
    }

    public enum EMetier
    {
        AUBERGISTE,
        BOUCHER,
        BOULANGER,
        CHASSEUR,
        COMMERCANT,
        COMMIS,
        CUISINIER,
        DIRIGEANT,
        ERUDIT,
        FERMIER,
        MINEUR,
        PECHEUR,
        PRETRE,
        SANS,
        SOLDAT,
        TAVERNIER,
        TANNEUR
    }

    public enum ETailleCommunaute
    {
        VILLAGE,
        VILLE,
        PLACE_FORTE
    }
}
