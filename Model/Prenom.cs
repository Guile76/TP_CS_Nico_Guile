﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Prenom
    {
        // Attributs
        public long Id { get; set; }
        public string Proposition { get; set; }
        public ERace Race { get; set; }
        public ESexe Sexe { get; set; }


        public override string ToString()
        {
            return Proposition;
        }
        // Constructeur
        public Prenom()
        {

        }

        public Prenom(string proposition, ERace race, ESexe sexe)
        {
            Id = 0;
            Proposition = proposition;
            Race = race;
            Sexe = sexe;
        }

    }
}
