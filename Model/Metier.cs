﻿
namespace Model
{
    public class Metier
    {
        // Attributs
        public EMetier Intitule { get; set; }
        public int AgeMinimum { get; set; }

        // Constructeur
        public Metier()
        {

        }

        public Metier(EMetier intitule, int ageMinimum)
        {
            Intitule = intitule;
            AgeMinimum = ageMinimum;
        }

    }
}
